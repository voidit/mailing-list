import React from 'react';

const ContactDetail = (props) => {
    const { name, email, phone, avatar } = props.contact;

    return (
        <div className="panel-body media">
            <div className="media-left">
                <a href="#">
                    <img className="media-object" src={ avatar } alt={ name } />
                </a>
            </div>
            <div className="media-body">
                <h4 className="media-heading">{ name }</h4>
                <p className="list-group-item-text"><a href={ 'mailto:'+email }>{ email }</a></p>
                <p className="list-group-item-text">{ phone }</p>
            </div>
        </div>
    );
};

export default ContactDetail;
