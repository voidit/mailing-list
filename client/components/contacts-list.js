import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Contacts } from '../../imports/collections/contacts';
import ContactDetail from './contact-detail';

const PER_PAGE = 2;

class ContactsList extends Component {
    componentWillMount() {
        this.page = 1;
    }

    handleButtonClick(){
        this.page += 1;
        Meteor.subscribe('contacts', PER_PAGE * this.page);
    }

    render() {
        return (
            <div>
                <div className="contacts-list">
                    { this.props.contacts.map(contact => {
                        return (
                            <div className="panel panel-default contacts-card" key={ contact._id }>
                                <ContactDetail contact={ contact }/>
                            </div>
                        )
                    })
                    }
                </div>
                <button
                    onClick={this.handleButtonClick.bind(this)}
                    className="btn btn-primary"
                     role="button">
                    Load more
                </button>
            </div>
        );
    }
}

export default createContainer(() => {
    //setup subscription
    Meteor.subscribe('contacts', PER_PAGE);
    
    //return an object, that will be sent to ContactsList as props
    return { contacts: Contacts.find({}).fetch() };

}, ContactsList);