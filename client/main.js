import React from 'react';
import { render } from 'react-dom';

import ContactsList from './components/contacts-list';

const App = () => {
    return <div><ContactsList /></div>;
}

Meteor.startup(() => {
    render(<App/>, document.querySelector('.container'));
});