import { Meteor } from 'meteor/meteor';
import _ from 'lodash';
import { image, helpers } from 'faker';
import { Contacts } from '../imports/collections/contacts';

Meteor.startup(() => {
  // generate data
    //check if empty
    const numberRecords = Contacts.find({}).count();
    console.log(numberRecords);

    if(!numberRecords){
        //generate fake records
        _.times(300, () => {
            const { name, email, phone } = helpers.createCard();

            Contacts.insert({
                name, email, phone,
                avatar: image.avatar()
            });
        });
    }

    Meteor.publish('contacts', ( per_page ) => {
        return Contacts.find({}, { limit: per_page });
    })
});
